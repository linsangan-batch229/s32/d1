let http = require('http');

// dummy data

let directory = [
    {
        "name" : "brandon",
        "email" : "brandon@mail.com"
    },
    {
        "name" : "jobert",
        "email" : "jobert@mail.com"
    }
];

http.createServer((request, response) => {

    // route for returning all items upon recieving GET method
    if (request.url == '/users' && request.method == 'GET'){
        response.writeHead(200, {'Content-Type' : 'application/json'});
        
        //strigify -> JSON to string data type
        //input has to be a string for application to read the incoming
        //properly
        response.write(JSON.stringify(directory));

        //ends the response process
        response.end();
    }

    //POST METHOD - add data to the dummy data
    if (request.url == '/users' && request.method == 'POST'){

        //declare and initialize reqBody variable to an empty string
        //this will act as a place holder for the data to be created later.
        let reqBody = '';

        //this is where data insertion happens to our mock/dummy data
        //nodemon js
        request.on("data", function(data){
            //assigns the data retrieved from the stream to reqBody
            reqBody += data;
        });

        //response end step - only run after the request has completely
        //been set
        request.on("end", () => {
            console.log(typeof reqBody);

            //converts the strings to JSON
            reqBody = JSON.parse(reqBody);

            // create a new oject representing the new mock data record
            let newUser = {
                'name' : reqBody.name,
                'email' : reqBody.email
            };

            directory.push(newUser);
            console.log(directory);

            //response.writeHead()
            response.writeHead(200, {'Content-Type' : 'application/json'});
            //to show the data
            response.write(JSON.stringify(newUser));
            
            //end the process
            response.end();
        });

    }


    

}).listen(3000);

console.log('CRUD is now running at port 3000');